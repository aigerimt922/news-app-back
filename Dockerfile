FROM tomcat:9-jdk17-openjdk

WORKDIR /usr/local/tomcat/webapps/

COPY target/news-app-back.war news-app-back.war

EXPOSE 8080

CMD ["catalina.sh", "run"]