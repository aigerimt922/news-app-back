package org.epam;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.loader.WebappLoader;
import org.apache.catalina.startup.Tomcat;
import org.epam.config.AppInitializer;
import org.springframework.context.annotation.ComponentScan;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

public class MyApplication {
    public static void main(String[] args) throws LifecycleException, ServletException {
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(8080);

        Context rootContext = tomcat.addContext("", "/");
        WebappLoader loader = new WebappLoader(Thread.currentThread().getContextClassLoader());
        rootContext.setLoader(loader);

        // Create an instance of AppInitializer and call its onStartup method
        AppInitializer initializer = new AppInitializer();
        ServletContext servletContext = rootContext.getServletContext();
        initializer.onStartup(servletContext);

        tomcat.start();
        tomcat.getServer().await();
    }
}
