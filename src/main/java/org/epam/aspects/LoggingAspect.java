package org.epam.aspects;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

    @Around("@annotation(org.epam.aspects.annotations.Loggable) && execution(* org.epam.controllers.*.*(..))")
    public Object logMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();

        // Get the class and method names
        String className = joinPoint.getTarget().getClass().getSimpleName();
        String methodName = methodSignature.getName();

        // Log the method entry
        Logger logger = LoggerFactory.getLogger(className);
        logger.debug("Entering method {} in class {}", methodName, className);

        // Execute the method and log the exit
        Object result = joinPoint.proceed();
        logger.debug("Exiting method {} in class {}", methodName, className);

        // Return the method result
        return result;
    }
}
