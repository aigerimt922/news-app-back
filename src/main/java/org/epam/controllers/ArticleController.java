package org.epam.controllers;

import org.apache.log4j.Logger;
import org.epam.aspects.annotations.Loggable;
import org.epam.dto.ArticleDetails;
import org.epam.dto.ArticleForm;
import org.epam.dto.ResponseMessage;
import org.epam.exceptions.InvalidRequestException;
import org.epam.services.ArticleService;
import org.epam.util.CustomPageable;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping("/api/articles")
public class ArticleController {

    private static final Logger log = Logger.getLogger(ArticleController.class);

    private final ArticleService articleService;


    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @GetMapping()
    @ResponseBody
    @Loggable
    public ResponseEntity<Page<ArticleDetails>> getAllArticles(
            @RequestParam(value = "q", required = false) String keyword,
            CustomPageable page) {

        log.debug(String.format("get all articles of q - %s, page - %s", keyword, page));

        Page<ArticleDetails> articles = articleService.searchArticlesByCriteria(keyword, page);

        log.debug(String.format("get articles size %s", articles.getTotalElements()));

        return new ResponseEntity<>(articles, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    @Loggable
    public ResponseEntity<ArticleDetails> getArticle(@PathVariable("id") Long id) {

        log.debug(String.format("get articles info by id %s", id));

        ArticleDetails article = articleService.findArticleById(id);

        log.debug(String.format("get article %s", article));

        return new ResponseEntity<>(article, HttpStatus.OK);
    }

    @PostMapping()
    @ResponseBody
    @Loggable
    public ResponseEntity<ResponseMessage> createArticle(@RequestBody @Valid ArticleForm article, BindingResult errResult) {

        log.debug("create a new article");
        if (errResult.hasErrors()) {
            throw new InvalidRequestException(errResult);
        }

        ArticleDetails saved = articleService.saveArticle(article);

        log.debug(String.format("saved article id is %s", saved.getId()));

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/articles" + "/{id}")
                .buildAndExpand(saved.getId())
                .toUri()
        );
        return new ResponseEntity<>(ResponseMessage.success("article.created"), headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseBody
    @Loggable
    public ResponseEntity<ResponseMessage> deleteArticleById(@PathVariable("id") Long id) {

        log.debug(String.format("delete article by id %s", id));

        articleService.deleteArticleById(id);

        return new ResponseEntity<>(ResponseMessage.success("article.updated"), HttpStatus.NO_CONTENT);
    }
}
