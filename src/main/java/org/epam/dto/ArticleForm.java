package org.epam.dto;

import org.epam.entity.Article;
import org.epam.util.DTOUtils;

import java.io.Serial;
import java.io.Serializable;

public class ArticleForm implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    private String title;

    private String content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "PostForm{" + "title=" + title + ", content=" + content + '}';
    }

    public Article toEntity() {
        return DTOUtils.map(this, Article.class);
    }

}
