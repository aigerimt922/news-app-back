package org.epam.entity;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
