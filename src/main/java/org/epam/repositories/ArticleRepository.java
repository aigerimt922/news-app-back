package org.epam.repositories;

import org.epam.entity.Article;
import org.epam.util.CustomPageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long>,
        JpaSpecificationExecutor<Article> {

    @Query(value = "SELECT a FROM Article a",
            countQuery = "SELECT count(*) FROM Article")
    Page<Article> findAllArticles(@PageableDefault(size = 10, sort = "id", direction = Sort.Direction.DESC) CustomPageable pageable);
}
