package org.epam.repositories;



import org.epam.entity.Article;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;



public final class ArticleSpecification {

    private ArticleSpecification() {
        throw new InstantiationError("Must not instantiate this class");
    }

    public static Specification<Article> filterByKeyword(
            final String keyword) {
        return (Root<Article> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (StringUtils.hasText(keyword)) {
                predicates.add(
                        cb.or(
                                cb.like(root.get("title"), "%" + keyword + "%"),
                                cb.like(root.get("content"), "%" + keyword + "%")
                        )
                );
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

}
