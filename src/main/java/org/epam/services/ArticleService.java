package org.epam.services;

import org.epam.dto.ArticleDetails;
import org.epam.dto.ArticleForm;
import org.epam.util.CustomPageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ArticleService {

    Page<ArticleDetails> searchArticlesByCriteria(String q, CustomPageable page);

    ArticleDetails saveArticle(ArticleForm form);

    ArticleDetails findArticleById(Long id);

    void deleteArticleById(Long id);
}
