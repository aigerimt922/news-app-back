package org.epam.services;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface CustomUserDetailsService extends UserDetailsService {
    UserDetails loadUserByUsername(String usernameOrEmail);
    UserDetails loadUserById(Long id);
}
