package org.epam.services;

import org.epam.entity.Role;
import org.epam.entity.RoleName;

public interface RoleService {

    Role getByName(RoleName name);
}
