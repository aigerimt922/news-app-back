package org.epam.services;

import org.epam.dto.UserResponse;
import org.epam.entity.User;

public interface UserService {

    boolean existsByUsername(String username);

    UserResponse getCurrentUser();

    void saveUser(User user);
}
