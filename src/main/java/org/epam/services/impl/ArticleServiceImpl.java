package org.epam.services.impl;

import com.mysql.cj.util.StringUtils;
import org.apache.log4j.Logger;
import org.epam.dto.ArticleDetails;
import org.epam.dto.ArticleForm;
import org.epam.entity.Article;
import org.epam.exceptions.ResourceNotFoundException;
import org.epam.repositories.ArticleRepository;
import org.epam.repositories.ArticleSpecification;
import org.epam.services.ArticleService;
import org.epam.util.CustomPageable;
import org.epam.util.DTOUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;


@Service
public class ArticleServiceImpl implements ArticleService {

    private static final Logger log = Logger.getLogger(ArticleServiceImpl.class);

    private final ArticleRepository articleRepository;

    public ArticleServiceImpl(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public Page<ArticleDetails> searchArticlesByCriteria(String q, CustomPageable page) {

        log.debug(String.format("search Articles by keyword %s, page %s", q, page));

        Page<Article> articles = StringUtils.isNullOrEmpty(q)
                ? articleRepository.findAllArticles(page)
                : articleRepository.findAll(ArticleSpecification.filterByKeyword(q), page);

        log.debug(String.format("get first article %s",
                articles.stream().findFirst().orElse(null)));

        return DTOUtils.mapPage(articles, ArticleDetails.class);
    }

    public ArticleDetails saveArticle(ArticleForm form) {

        log.debug(String.format("save Article %s", form));

        Article article = DTOUtils.map(form, Article.class);

        Article saved = articleRepository.save(article);

        log.debug(String.format("saved Article is %s", saved));

        return DTOUtils.map(saved, ArticleDetails.class);

    }

    public ArticleDetails updateArticle(Long id, ArticleForm form) {
        log.debug(String.format(" update Article %s", form));

        Article article = articleRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(id));
        DTOUtils.mapTo(form, article);

        Article saved = articleRepository.save(article);

        log.debug(String.format(" updated Article %s", saved));

        return DTOUtils.map(saved, ArticleDetails.class);
    }

    public ArticleDetails findArticleById(Long id) {
        log.debug(String.format(": find Article by id: %s", id));

        Article article = articleRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(id));

        return DTOUtils.map(article, ArticleDetails.class);
    }

    public void deleteArticleById(Long id) {

        log.debug(String.format("find post by id %s", id));

        articleRepository.findById(id)
                .ifPresentOrElse(articleRepository::delete,
                        () -> {
                            throw new ResourceNotFoundException(id);
                        });
    }
}
