package org.epam.services.impl;

import org.epam.auth.UserPrincipal;
import org.epam.entity.User;
import org.epam.exceptions.ResourceNotFoundException;
import org.epam.repositories.UserRepository;
import org.epam.services.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailsServiceImpl implements CustomUserDetailsService {
    @Autowired
    UserRepository userRepository;

    public CustomUserDetailsServiceImpl() {
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
        // Try to find the user by username or email
        User user = userRepository.findByUsername(usernameOrEmail)
                .orElseThrow(() ->
                        new UsernameNotFoundException("User not found with username or email : " + usernameOrEmail)
                );

        // Build the user details object using the found user and their roles
        return UserPrincipal.create(user);
    }

    @Transactional
    public UserDetails loadUserById(Long id) {
        // Find the user by their id
        User user = userRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException(id)
        );

        // Build the user details object using the found user and their roles
        return UserPrincipal.create(user);
    }
}
