package org.epam.services.impl;

import org.epam.entity.Role;
import org.epam.entity.RoleName;
import org.epam.repositories.RoleRepository;
import org.epam.services.RoleService;
import org.springframework.stereotype.Service;

import java.util.concurrent.Callable;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role getByName(RoleName name) {
        return roleRepository.findByName(name).orElseGet(() -> {
            // Create user role
            Role userRole = new Role(RoleName.ROLE_USER);
            return roleRepository.save(userRole);
        });
    }
}
