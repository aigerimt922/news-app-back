package org.epam.services.impl;

import org.epam.dto.UserResponse;
import org.epam.entity.Role;
import org.epam.entity.RoleName;
import org.epam.entity.User;
import org.epam.repositories.RoleRepository;
import org.epam.repositories.UserRepository;
import org.epam.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collections;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           PasswordEncoder passwordEncoder,
                           RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }

    @PostConstruct
    public void init() {
        // Create admin role
        Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN).orElseGet(() -> {
            Role userRole = new Role(RoleName.ROLE_ADMIN);
            return roleRepository.save(userRole);
        });

        User admin = userRepository.findByUsername("admin@example.com").orElseGet(() -> {
            User newUser = new User("admin@example.com",
                    passwordEncoder.encode("adminpassword"), "admin");
            newUser.setRoles(Collections.singleton(adminRole));
            newUser.setRoles(Collections.singleton(adminRole));
            return newUser;
        });

        userRepository.save(admin);
    }

    @Override
    public boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    @Override
    public UserResponse getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        User user = userRepository.findByUsername(username)
                .orElseThrow(() ->
                        new UsernameNotFoundException("User not found with username or email : " + username));

        return new UserResponse(user.getId(), user.getUsername(), user.getName());

    }

    @Override
    public void saveUser(User user) {
        userRepository.save(user);
    }


}
