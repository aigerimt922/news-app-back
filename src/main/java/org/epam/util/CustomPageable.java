package org.epam.util;


import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.io.Serializable;

public class CustomPageable implements Pageable, Serializable {

    private final int page;
    private final int size;
    private final Sort sort;

    public CustomPageable(Integer page, Integer size, Sort sort) {
        this.page = page != null ? page : 0;
        this.size = size != null ? size : 10;
        this.sort = sort != null ? sort : Sort.unsorted();
    }

    public CustomPageable withPage(int page) {
        return new CustomPageable(page, this.size, this.sort);
    }

    @Override
    public int getPageNumber() {
        return page;
    }

    @Override
    public int getPageSize() {
        return size;
    }

    @Override
    public long getOffset() {
        return (long) page * (long) size;
    }

    @Override
    public Sort getSort() {
        return sort;
    }

    @Override
    public Pageable next() {
        return new CustomPageable(getPageNumber() + 1, getPageSize(), getSort());
    }

    public CustomPageable previous() {
        return hasPrevious() ? new CustomPageable(getPageNumber() - 1, getPageSize(), getSort()) : this;
    }

    @Override
    public Pageable previousOrFirst() {
        return hasPrevious() ? previous() : first();
    }

    @Override
    public Pageable first() {
        return new CustomPageable(0, getPageSize(), getSort());
    }

    @Override
    public boolean hasPrevious() {
        return page > 0;
    }
}
