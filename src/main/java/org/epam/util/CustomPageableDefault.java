package org.epam.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER})
public @interface CustomPageableDefault {
    int page() default 0;

    int size() default 10;

    String[] sort() default {"id,desc"};
}
