package org.epam.util;

import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.jaxb.SpringDataJaxb;

public final class DTOUtils {

    private static final ModelMapper INSTANCE = new ModelMapper();

    private DTOUtils() {
        throw new InstantiationError( "Must not instantiate this class" );
    }

    public static <S, T> T map(S source, Class<T> targetClass) {
        return INSTANCE.map(source, targetClass);
    }

    public static <S, T> void mapTo(S source, T dist) {
        INSTANCE.map(source, dist);
    }

    public static <S, T> List<T> mapList(List<S> source, Class<T> targetClass) {
        List<T> list = new ArrayList<>();
        for (S s : source) {
            T target = INSTANCE.map(s, targetClass);
            list.add(target);
        }

        return list;
    }

    public static <S, T> Page<T> mapPage(Page<S> source, Class<T> targetClass) {
        List<S> sourceList = source.getContent();

        List<T> list = new ArrayList<>();
        for (S s : sourceList) {
            T target = INSTANCE.map(s, targetClass);
            list.add(target);
        }

        return new PageImpl<>(list, PageRequest.of(source.getNumber(), source.getSize() < 1 ? 10 : source.getSize(), source.getSort()),
                source.getTotalElements());
    }
}
