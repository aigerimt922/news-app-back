package org.epam.services.impl;

import org.epam.dto.ArticleDetails;
import org.epam.dto.ArticleForm;
import org.epam.entity.Article;
import org.epam.exceptions.ResourceNotFoundException;
import org.epam.repositories.ArticleRepository;
import org.epam.util.CustomPageable;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ArticleServiceImplTest {

    @Mock
    private ArticleRepository articleRepository;

    @InjectMocks
    private ArticleServiceImpl articleService;

    @Test
    void searchArticlesByCriteria_shouldReturnPageOfArticles() {
        // given
        CustomPageable page = new CustomPageable(0, 10, Sort.unsorted());
        Page<Article> articles = new PageImpl<>(Collections.singletonList(new Article()));
        given(articleRepository.findAllArticles(page)).willReturn(articles);

        // when
        Page<ArticleDetails> result = articleService.searchArticlesByCriteria(null, page);

        // then
        assertEquals(1, result.getContent().size());
        assertEquals(ArticleDetails.class, result.getContent().get(0).getClass());
    }

    @Test
    void searchArticlesByCriteria_withKeyword_shouldReturnPageOfArticles() {
        // given
        CustomPageable page = new CustomPageable(0, 10, Sort.unsorted());
        Page<Article> articles = new PageImpl<>(Collections.singletonList(new Article()));
        given(articleRepository.findAll(any(Specification.class), eq(page))).willReturn(articles);

        // when
        Page<ArticleDetails> result = articleService.searchArticlesByCriteria("test", page);

        // then
        assertEquals(1, result.getContent().size());
        assertEquals(ArticleDetails.class, result.getContent().get(0).getClass());
    }

    @Test
    void saveArticle_shouldSaveAndReturnArticleDetails() {
        // given
        ArticleForm form = new ArticleForm();
        Article article = new Article();
        given(articleRepository.save(any(Article.class))).willReturn(article);

        // when
        ArticleDetails result = articleService.saveArticle(form);

        // then
        assertEquals(ArticleDetails.class, result.getClass());
    }

    @Test
    void updateArticle_shouldUpdateAndReturnArticleDetails() {
        // given
        Long id = 1L;
        ArticleForm form = new ArticleForm();
        Article article = new Article();
        given(articleRepository.findById(id)).willReturn(Optional.of(article));
        given(articleRepository.save(article)).willReturn(article);

        // when
        ArticleDetails result = articleService.updateArticle(id, form);

        // then
        assertEquals(ArticleDetails.class, result.getClass());
    }

    @Test
    void updateArticle_withNonexistentId_shouldThrowResourceNotFoundException() {
        // given
        Long id = 1L;
        ArticleForm form = new ArticleForm();
        given(articleRepository.findById(id)).willReturn(Optional.empty());

        // when / then
        assertThrows(ResourceNotFoundException.class, () -> articleService.updateArticle(id, form));
    }

    @Test
    void findArticleById_shouldReturnArticleDetails() {
        // given
        Long id = 1L;
        Article article = new Article();
        given(articleRepository.findById(id)).willReturn(Optional.of(article));

        // when
        ArticleDetails result = articleService.findArticleById(id);

        // then
        assertEquals(ArticleDetails.class, result.getClass());
    }

    @Test
    void findArticleById_withNonexistentId_shouldThrowResourceNotFoundException() {
        // given
        Long id = 1L;
        given(articleRepository.findById(id)).willReturn(Optional.empty());

        // when / then
        assertThrows(ResourceNotFoundException.class, () -> articleService.findArticleById(id));
    }

    @Test
    void deleteArticleById_shouldDeleteIfExists() {
        // given
        Long id = 1L;
        Article article = new Article();
        given(articleRepository.findById(id)).willReturn(Optional.of(article));

        // when
        articleService.deleteArticleById(id);

        // then
        verify(articleRepository).delete(article);
    }
}